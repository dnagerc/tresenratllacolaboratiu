# TresEnRatllaColaboratiu

Board
    Guarda las piezas que hay colocadas y las que no
    
RefreshBoard
    

Pieces
Funcion que te imprime 4 bolas a la derecha y 4 a la izquierda

DragNDropPiece
Funcion que te coloca una ficha en el tablero
    - Como?
        Al clicar una pieza del lateral, mostraremos un mensaje 
        que indique que se ha clicado la pieza y le pediremos el lugar donde colocar la pieza
        al usuario.
        - AskPiece
TurnOfUser
    - Mostrar el torn del jugador

PutPieceInBoard
    - Coge la posicion que se ha indicado en DragNDropPiece i la pone en el tablero.
    - Una vez se ha colocado una ficha en el tablero, desaparece de los laterales.
    RefreshBoard
    PrintBoard -> recorre una array de valores del tablero

Control de victories
    UserStatus
        - Win
        - Draw


Control de errors:
    - Si a la posició que es demana a AskPiece està a la array del Board, hem de mostrar un error
    i tot seguit, tornar a demanar.
    - Peça fora del tauler, missatge d'error

Fitxes:
    - Imatge
    - Elipse, Circle, etc.




