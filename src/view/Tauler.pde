import java.util.ArrayList;
import java.util.List;

PImage bg;
PImage img, wallpaper;
PFont f;
ArrayList<Fitxa> fitxes = new ArrayList<Fitxa>();
int turn = 0;
void setup() {
  size(1024, 780);
  loadImages();
  f = createFont("Arial", 24);
  textFont(f);
  image(wallpaper, 0, 0, 1024, 780);
  DrawCeles();
  DrawFitxes();
  cursor(CROSS);
}

void draw() {
  updateCeles();
}

void loadImages() {
  img = loadImage("cross.jpg"); 
  wallpaper = loadImage("wallpaper.jpg"); 
}

void mouseClicked() {
    boolean valid = false;
    // Check Bola o X
    
    
    
    
    
    for (Fitxa fitxa : fitxes) {
      if (mouseX > fitxa.getX() && mouseX < fitxa.getX()+130){
        if (mouseY > fitxa.getY() && mouseY < fitxa.getY()+130){
//          fitxa.setColor();
          // rodones
//          fitxa.setValue(2);
          // X
          if (turn++ % 2 == 0) {
            fitxa.setValue(2);
          } else {
            fitxa.setValue(1);          
          }
          System.out.println(fitxa.getY());
          System.out.println("Mouse: "+mouseY);
          
          System.out.println(fitxa.getX());          
          System.out.println("Mouse: "+mouseX);
          valid = true;
        }
      }
    }
    if(!valid) {
      DrawTurn("Vigila, has clicat fora del tauler");
    }
}

void updateCeles() {
    for (Fitxa fitxa : fitxes) {
      fitxa.draw();
    }
}
void DrawCeles() {
  int[][] celes = {
    {325, 195, 130, 130},
    {325, 325, 130, 130},
    {325, 455, 130, 130},  
    {455, 195, 130, 130},
    {455, 325, 130, 130},
    {455, 455, 130, 130},
    {585, 195, 130, 130},
    {585, 325, 130, 130},    
    {585, 455, 130, 130}
  };
  int x, y, ample, alt; 
  for (int[] cela : celes) {
    x = cela[0];
    y = cela[1];
    ample = cela[2];
    alt = cela[3];
    Fitxa fitxa = new Fitxa(x, y, ample, alt, #333333);
    fitxes.add(fitxa);
  }
}

void DrawFitxes() {    
    int[][] jugadorO = {
      {195, 243, 96, 96},
      {195, 341, 96, 96},
      {195, 439, 96, 96},
      {195, 537, 96, 96}
    };
    
    int[][] jugadorX = {
      {797, 195, 96, 96},
      {797, 293, 96, 96},
      {797, 391, 96, 96},
      {797, 489, 96, 96}
    };

    int x, y, ample, alt;
    // Imprimir els cercles
    for (int[] cela : jugadorO) {
      x = cela[0];
      y = cela[1];
      ample = cela[2];
      alt = cela[3];

      // Config circle
      stroke(153);
      fill(#ff8535);  
      ellipse(x, y, ample, alt);
    }
        
    // Imprimir les X amb imatge
    for (int[] cela : jugadorX) {
      x = cela[0];
      y = cela[1];
      ample = cela[2];
      alt = cela[3];

      // Config image
      stroke(153);
      fill(#ff8535);  
      image(img, x, y, ample, alt);
    }
}

void DrawTurn(String turn) {
      fill(#000000);
      text(turn, 325, 97);
}
