public class Fitxa {
  private int x; 
  private int y;
  private int ample;
  private int alt; 
  private int colors;
  private int value;
  public Fitxa (int x, int y, int ample, int alt, int colors) {
    this.x = x;
    this.y = y;
    this.ample = ample;
    this.alt = alt;
    this.colors = colors;
    rect(x, y, ample, alt); 
  }
  void draw() {
    if (value == 1) {
      image(img, x, y, ample, alt);
    } else if (value == 2) {
      fill(#555555);  
      rect(x, y, ample, alt); 
      fill(#ff8535);  
      ellipse(x+65, y+65, ample, alt);
    } else {
          stroke(#a600f7);

      fill(colors);  
      rect(x, y, ample, alt); 
    }
  }
  public int getX() {
    return x;
  }
  public void setColor() {
    colors = #555555;
  }
  public void setValue(int value) {
    this.value = value;
  }
  public int getY() {
    return y;
  }
  public int getAmple() {
    return ample;
  }
  public int getAlt() {
    return alt;
  }
}
